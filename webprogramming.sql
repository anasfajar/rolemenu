-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for webprogramming
CREATE DATABASE IF NOT EXISTS `webprogramming` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `webprogramming`;

-- Dumping structure for table webprogramming.admin_menus
CREATE TABLE IF NOT EXISTS `admin_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table webprogramming.admin_menus: ~37 rows (approximately)
/*!40000 ALTER TABLE `admin_menus` DISABLE KEYS */;
INSERT INTO `admin_menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Manage Investor', '2019-05-15 04:13:48', '2020-02-03 11:55:21'),
	(2, 'Manage Proyek', '2019-05-15 05:45:00', '2019-05-15 05:45:00'),
	(3, 'Manage Marketer', '2019-05-15 05:45:32', '2019-05-15 05:45:32'),
	(4, 'Manage Imbal Hasil', '2019-05-15 05:45:50', '2019-05-15 05:45:50'),
	(5, 'Manage Carousel', '2019-05-15 05:46:06', '2019-05-15 05:46:06'),
	(6, 'Export Report', '2019-05-15 05:46:35', '2019-05-15 05:46:35'),
	(7, 'News', '2019-05-15 05:46:43', '2019-05-15 05:46:43'),
	(8, 'Data E Collection BNI', '2019-05-15 05:48:05', '2019-05-15 05:48:05'),
	(9, 'Convert CSV to JSON', '2019-05-15 05:48:18', '2019-05-15 05:48:18'),
	(11, 'Manage Users', '2019-05-16 07:06:57', '2019-05-16 07:35:10'),
	(12, 'Manage Message', '2019-05-16 07:08:04', '2019-05-16 07:08:04'),
	(13, 'Penarikan Dana', '2019-05-16 07:13:49', '2019-05-16 07:13:49'),
	(14, 'Manage Khazanah', '2019-05-28 02:09:19', '2019-05-28 02:09:19'),
	(15, 'Manage Penghargaan', '2019-05-28 02:13:57', '2019-05-28 02:13:57'),
	(16, 'Manage Menu', '2019-06-03 05:46:47', '2019-06-03 05:46:47'),
	(17, 'Manage Imbal Hasil', '2019-09-02 03:49:16', '2019-09-02 03:49:16'),
	(18, 'Manage Version', '2019-10-24 06:12:27', '2019-10-24 06:12:27'),
	(19, 'Persyaratan Borrower', '2019-11-21 05:33:53', '2019-11-21 05:33:53'),
	(20, 'Scoring Borrower', '2019-11-21 05:37:04', '2019-11-21 05:37:04'),
	(21, 'Teks Notifikasi', '2019-12-02 08:10:00', '2019-12-02 08:10:00'),
	(22, 'Threshold Kontrak', '2019-12-02 08:10:33', '2019-12-02 08:10:33'),
	(24, 'Data Borrower', '2019-12-10 12:40:33', '2019-12-10 12:40:33'),
	(25, 'Log Aktifitas', '2020-02-03 08:39:19', '2020-02-03 08:39:19'),
	(27, 'Konten Manajemen Website', '2020-02-10 02:31:15', '2020-02-10 02:31:15'),
	(28, 'Pencairan Dana', '2020-02-11 03:15:10', '2020-02-11 03:15:10'),
	(29, 'Tarik Dana Pendana', '2020-02-18 14:07:31', '2020-02-18 14:07:31'),
	(30, 'Transfer Dana Pendana', '2020-02-19 04:11:58', '2020-02-19 04:11:58'),
	(31, 'Log Transaksi Bank RDL', '2020-02-19 16:31:45', '2020-02-19 16:31:45'),
	(32, 'Kelola Syarat Ketentuan', '2020-03-10 05:53:46', '2020-03-10 05:53:46'),
	(33, 'test imbal hasil', '2020-04-07 04:40:08', '2020-04-07 04:40:08'),
	(34, 'Data FDC', '2020-06-09 09:10:46', '2020-06-09 09:10:46'),
	(35, 'Kelola Properti', '2020-08-14 06:53:59', '2020-08-14 06:53:59'),
	(36, 'Kelola Pengaju KPR', '2020-08-27 06:59:14', '2020-08-27 06:59:14'),
	(37, 'Kelola Dana Penerima Pendanaan', '2020-10-20 11:17:48', '2020-10-20 11:17:48'),
	(38, 'Kelola Persetujuan', '2020-11-13 02:10:57', '2020-11-13 02:12:18'),
	(39, 'Analis', '2020-11-23 05:53:29', '2020-11-23 05:53:29'),
	(43, 'Laporan Keuangan', '2020-12-15 03:10:06', '2020-12-15 03:10:06');
/*!40000 ALTER TABLE `admin_menus` ENABLE KEYS */;

-- Dumping structure for table webprogramming.admin_menu_items
CREATE TABLE IF NOT EXISTS `admin_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) unsigned NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `class` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu` int(10) unsigned DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `admin_menu_items_menu_foreign` (`menu`),
  CONSTRAINT `admin_menu_items_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `admin_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table webprogramming.admin_menu_items: ~70 rows (approximately)
/*!40000 ALTER TABLE `admin_menu_items` DISABLE KEYS */;
INSERT INTO `admin_menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`, `role_id`) VALUES
	(1, 'Kelola Pendana', '', 0, 0, NULL, 1, 0, '2019-05-15 06:35:00', '2021-04-07 03:58:23', 1),
	(2, 'Verifikasi Pendana', 'admin/investor/verifikasi', 1, 1, NULL, 1, 1, '2019-05-15 06:42:50', '2020-02-03 11:55:58', 0),
	(3, 'Data Pendana', 'admin/investor/manage', 1, 2, NULL, 1, 1, '2019-05-15 06:53:12', '2020-02-03 11:56:12', 0),
	(5, 'Kelola Proyek', '', 0, 0, NULL, 2, 0, '2019-05-16 03:23:59', '2020-02-03 11:58:39', 1),
	(6, 'Tambah Proyek', 'admin/proyek/create', 5, 1, NULL, 2, 1, '2019-05-16 03:24:57', '2020-02-03 11:58:39', 0),
	(7, 'Atur Proyek', 'admin/proyek/manage', 5, 2, NULL, 2, 1, '2019-05-16 03:26:09', '2020-02-03 11:58:39', 0),
	(8, 'Mutasi Proyek', 'admin/proyek/mutasi', 5, 3, NULL, 2, 1, '2019-05-16 03:27:11', '2020-02-03 11:58:39', 0),
	(9, 'Kelola Wiraniaga', 'http://', 0, 0, NULL, 3, 0, '2019-05-16 03:36:56', '2020-02-03 12:00:08', 2),
	(10, 'Atur Wiraniaga', 'admin/marketer/manage', 9, 1, NULL, 3, 1, '2019-05-16 03:37:51', '2020-02-03 12:01:29', 0),
	(11, 'Mutasi Wiraniaga', 'admin/marketer/mutasi', 9, 2, NULL, 3, 1, '2019-05-16 03:38:52', '2020-02-03 12:01:29', 0),
	(12, 'Manage Imbal Hasil', 'admin/proyek/manage_payout_data', 0, 0, NULL, 4, 0, '2019-05-16 03:43:02', '2019-05-16 03:45:21', 0),
	(13, 'Kelola Karosel', 'admin/manage_carousel', 0, 0, NULL, 5, 0, '2019-05-16 06:59:00', '2020-02-03 12:02:43', 0),
	(14, 'Kelola Berita', 'http://', 0, 0, NULL, 7, 0, '2019-05-16 07:00:39', '2020-02-03 12:03:13', 0),
	(15, 'Tambah Berita', 'admin/news', 14, 1, NULL, 7, 1, '2019-05-16 07:00:53', '2020-02-03 12:03:34', 0),
	(16, 'Kelola Berita', 'admin/news/list', 14, 2, NULL, 7, 1, '2019-05-16 07:01:02', '2020-02-03 12:03:34', 0),
	(17, 'Ekspor Laporan', 'http://', 0, 0, NULL, 6, 0, '2019-05-16 07:02:43', '2020-02-03 12:04:23', 0),
	(18, 'Ekspor Proyek', 'admin/proyek/proyek_eksport_manage', 17, 1, NULL, 6, 1, '2019-05-16 07:03:46', '2020-02-03 12:04:35', 0),
	(19, 'Data (E-Collection) BNI', 'admin/e_coll_bni', 0, 0, NULL, 8, 0, '2019-05-16 07:04:48', '2020-02-03 12:05:01', 0),
	(20, 'Konversi CSV to JSON', 'admin/convertCsvToJson', 0, 0, NULL, 9, 0, '2019-05-16 07:05:39', '2020-02-03 12:05:34', 0),
	(21, 'Kelola Pengguna', 'admin/manage', 0, 0, NULL, 11, 0, '2019-05-16 07:07:04', '2020-02-03 12:05:57', 0),
	(22, 'Kelola Pesan', 'http://', 0, 0, NULL, 12, 0, '2019-05-16 07:08:10', '2020-02-03 12:06:13', 0),
	(23, 'Perpesanan (Broadcast)', 'admin/messagging/broadcast', 22, 1, NULL, 12, 1, '2019-05-16 07:08:21', '2020-02-03 12:06:35', 0),
	(24, 'Kirim Email', 'admin/messagging/send_mail', 22, 2, NULL, 12, 1, '2019-05-16 07:08:28', '2020-02-03 12:06:44', 0),
	(25, 'Penarikan Dana', '', 0, 0, NULL, 13, 0, '2019-05-16 07:13:56', '2020-02-03 12:08:08', 0),
	(26, 'Permohonan', 'admin/investor/request_withdraw', 25, 1, NULL, 13, 1, '2019-05-16 07:15:04', '2020-02-03 12:07:52', 0),
	(27, 'Terbayar', 'admin/investor/paid_withdraw', 25, 2, NULL, 13, 1, '2019-05-16 07:15:11', '2020-02-03 12:08:02', 0),
	(28, 'Gagal', 'admin/investor/failed_withdraw', 25, 3, NULL, 13, 1, '2019-05-16 07:15:14', '2020-02-03 12:08:11', 0),
	(29, 'Tautan Verifikasi Daftar', 'admin/link_register', 1, 4, NULL, 1, 1, '2019-05-23 06:15:35', '2021-04-07 03:58:16', 0),
	(30, 'Kelola Khazanah', 'admin/manage_khazanah', 0, 0, NULL, 14, 0, '2019-05-28 02:09:22', '2020-02-03 12:08:55', 0),
	(31, 'Kelola Penghargaan', 'admin/manage_penghargaan', 0, 0, NULL, 15, 0, '2019-05-28 02:14:04', '2020-02-03 12:09:32', 0),
	(32, 'Kelola Menu', 'rolemenu/kelolamenu', 0, 0, NULL, 16, 0, '2019-06-03 05:47:02', '2020-02-03 12:09:58', 1),
	(33, 'Proyek Selesai', 'admin/proyek/finish_proyek', 5, 4, NULL, 2, 1, '2019-06-25 05:56:35', '2020-02-03 11:58:39', 0),
	(34, 'Kelola Imbal Hasil', 'admin/proyek/manage_payout_data', 0, 0, NULL, 17, 0, '2019-09-02 03:49:45', '2020-02-03 12:11:01', 0),
	(35, 'Kelola Versi', 'admin/manageVersion', 0, 0, NULL, 18, 0, '2019-10-24 06:12:47', '2020-02-03 12:10:36', 0),
	(36, 'Persyaratan Penerima Pendanaan', 'admin/borrower/client/managePendanaanBorower', 0, 0, NULL, 19, 0, '2019-11-21 05:36:39', '2020-02-03 12:11:39', 0),
	(37, 'Penilaian Pengajuan Pendanaan', 'admin/borrower/client/scorringBorrower', 0, 0, NULL, 20, 0, '2019-11-21 05:37:27', '2020-11-24 05:28:41', 0),
	(38, 'Teks Notifikasi', 'admin/menu_teks', 0, 0, NULL, 21, 0, '2019-12-02 08:10:16', '2019-12-02 08:10:19', 0),
	(39, 'Threshold Kontrak', 'admin/menu_threshold', 0, 0, NULL, 22, 0, '2019-12-02 08:10:52', '2019-12-02 08:10:54', 0),
	(41, 'Data Penerima Pendanaan', '/admin/borrower/listBorrower', 0, 0, NULL, 24, 0, '2019-12-10 12:41:26', '2020-02-03 12:13:33', 0),
	(42, 'Log Aktifitas', 'admin/audit_trail', 0, 0, NULL, 25, 0, '2020-02-03 08:40:18', '2020-02-03 08:42:34', 0),
	(43, 'Konten Manajemen Website', 'admin/manage_cms', 0, 0, NULL, 27, 0, '2020-02-10 02:33:37', '2020-02-10 02:38:05', 0),
	(44, 'Testimoni Pendana', 'admin/ManageTestimoniPendana', 43, 1, NULL, 27, 1, '2020-02-10 02:34:40', '2020-02-10 02:38:05', 0),
	(45, 'Media Online', 'admin/ManageMediaOnline', 43, 2, NULL, 27, 1, '2020-02-10 02:35:35', '2020-02-10 09:13:33', 0),
	(46, 'Informasi Perusahaan', 'admin/ManageInformasiPerusahaan', 43, 3, NULL, 27, 1, '2020-02-10 02:36:34', '2020-02-10 02:38:05', 0),
	(47, 'Disclaimer', 'admin/ManageDisclaimerOJK', 43, 4, NULL, 27, 1, '2020-02-10 02:37:29', '2020-02-11 05:50:55', 0),
	(48, 'Pencairan Dana', 'admin/borrower/ListTableApproveDana', 0, 0, NULL, 28, 0, '2020-02-11 03:15:31', '2020-02-11 03:15:36', 0),
	(49, 'Footer Kebijakan', 'admin/ManagePrivacyPolicy', 43, 5, NULL, 27, 1, '2020-02-11 11:22:58', '2020-02-11 11:23:17', 0),
	(50, 'Tarik Dana Pendana', 'admin/tarik_dana_pendana', 0, 0, NULL, 29, 0, '2020-02-18 14:07:44', '2020-02-18 14:07:48', 0),
	(51, 'Transfer Dana Pendana', 'admin/transfer_dana_pendana', 0, 0, NULL, 30, 0, '2020-02-19 04:13:33', '2020-02-19 04:13:36', 0),
	(52, 'Log Transaksi Bank RDL', 'admin/RDLBank_transaction', 0, 0, NULL, 31, 0, '2020-02-19 16:33:07', '2020-02-19 16:33:07', 0),
	(53, 'Kelola Syarat Ketentuan', 'http://', 0, 0, NULL, 32, 0, '2020-03-10 05:55:34', '2020-03-10 05:57:18', 0),
	(54, 'Tambah Syarat Ketentuan', 'admin/SyaratKetentuan', 53, 1, NULL, 32, 1, '2020-03-10 05:56:21', '2020-03-10 05:57:18', 0),
	(55, 'List Syarat Ketentuan', 'admin/SyaratKetentuan', 53, 2, NULL, 32, 1, '2020-03-10 05:57:06', '2020-03-10 05:57:18', 0),
	(57, 'test imbal hasil', 'admin/imbalhasil/data_manage_payout', 0, 0, NULL, 33, 0, '2020-04-07 04:40:50', '2020-04-07 04:42:45', 0),
	(58, 'Data FDC', 'admin/view_upload_fdc', 0, 0, NULL, 34, 0, '2020-06-09 09:17:27', '2020-06-09 09:17:40', 0),
	(59, 'Kelola Properti', '', 0, 0, NULL, 35, 0, '2020-08-14 06:54:31', '2020-08-14 11:44:13', 0),
	(60, 'Tambah Properti', 'admin/properti/create', 59, 1, NULL, 35, 1, '2020-08-14 06:54:52', '2020-08-14 06:55:30', 0),
	(61, 'Atur Properti', 'admin/properti/manage', 59, 2, NULL, 35, 1, '2020-08-14 06:55:21', '2020-08-14 11:43:43', 0),
	(62, 'Kelola Pengaju KPR', '/admin/borrower/kpr/manage-data-pengajuan', 0, 0, NULL, 36, 0, '2020-08-27 06:59:21', '2020-08-27 07:00:56', 0),
	(64, 'Kelola Dana Penerima', '', 0, 0, NULL, 37, 0, '2020-10-20 11:21:06', '2020-11-16 07:19:52', 0),
	(65, 'Kelola Pencairan', 'admin/borrower/pencairan', 64, 2, NULL, 37, 1, '2020-10-20 11:22:47', '2020-11-16 07:19:44', 0),
	(66, 'Kelola Cicilan', 'admin/borrower/verifikasi_cicilan', 64, 1, NULL, 37, 1, '2020-10-21 03:13:44', '2020-11-16 07:19:44', 0),
	(67, 'Kelola Persetujuan', '', 0, 0, NULL, 38, 0, '2020-11-13 02:12:53', '2020-11-13 02:15:09', 0),
	(68, 'Persetujuan Pendanaan', 'admin/borrower/view_persetujuan_pendanaan', 67, 1, NULL, 38, 1, '2020-11-13 02:14:19', '2020-11-13 02:15:08', 0),
	(69, 'Persetujuan Pencairan', 'admin/borrower/view_persetujuan_pencairan', 67, 2, NULL, 38, 1, '2020-11-13 02:14:48', '2020-11-13 02:15:08', 0),
	(70, 'Laporan Keuangan', '/', 0, 0, NULL, 43, 0, '2020-12-15 03:10:43', '2020-12-15 03:11:57', 0),
	(71, 'Imbal Hasil', 'admin/borrower/view_laporan_imbal_hasil', 70, 1, NULL, 43, 1, '2020-12-15 03:11:02', '2020-12-15 03:19:49', 2),
	(72, 'Umur Piutang', 'admin/borrower/laporan_umur_piutang', 70, 2, NULL, 43, 1, '2020-12-15 03:11:13', '2020-12-15 03:12:16', 2),
	(74, 'Download Akad', 'admin/investor/akad', 1, 3, NULL, 1, 1, '2021-04-07 03:58:09', '2021-04-07 03:58:18', 2),
	(78, 'test menu', 'menu/menu', 0, 0, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
	(79, 'Menu Rahasia', 'menu/menu', 78, 0, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
	(80, 'Kelola Pengguna', 'rolemenu/kelolapengguna', 0, 0, NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);
/*!40000 ALTER TABLE `admin_menu_items` ENABLE KEYS */;

-- Dumping structure for table webprogramming.akses_pengguna
CREATE TABLE IF NOT EXISTS `akses_pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table webprogramming.akses_pengguna: ~0 rows (approximately)
/*!40000 ALTER TABLE `akses_pengguna` DISABLE KEYS */;
INSERT INTO `akses_pengguna` (`id`, `id_role`, `id_menu`, `created_at`) VALUES
	(7, 1, 1, '2021-07-24 10:49:13'),
	(8, 1, 5, '2021-07-24 10:49:13'),
	(9, 1, 32, '2021-07-24 10:49:13'),
	(10, 1, 80, '2021-07-24 10:49:13');
/*!40000 ALTER TABLE `akses_pengguna` ENABLE KEYS */;

-- Dumping structure for table webprogramming.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table webprogramming.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `nama_role`) VALUES
	(1, 'Admin'),
	(2, 'Analis');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table webprogramming.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table webprogramming.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
	(1, 'admintest', 'qweasd', 1),
	(2, 'analistest', 'qweasd', 2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
