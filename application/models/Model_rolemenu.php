<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_rolemenu extends CI_Model {


    public function get_akses_menu($parent,$id_role)
    {
            $this->db->select('*');
            $this->db->from('akses_pengguna a');
            $this->db->join('admin_menu_items b', 'a.id_menu = b.id');
            $this->db->where('b.parent',$parent);
            $this->db->where('a.id_role',$id_role);
            $query = $this->db->get();
            return $query->result();
    }

    public function get_akses_menu_child($parent)
    {
            $this->db->select('*');
            $this->db->from('admin_menu_items');
            $this->db->where('parent',$parent);
            $query = $this->db->get();
            return $query->result();
    }

    public function getMenuIn($menu){
            $this->db->select('*');
            $this->db->from('admin_menu_items');    
            $this->db->where_in('id', $menu);
            $query = $this->db->get();
            return $query->result();
    }

    public function getMenuNotIn($menu){
        $this->db->select('*');
        $this->db->from('admin_menu_items');    
        $this->db->where_not_in('id', $menu);
        $query = $this->db->get();
        return $query->result();
    }

    public function getMenu(){
        $this->db->select('*');
        $this->db->from('admin_menu_items');    
        $query = $this->db->get();
        return $query->result();
    }


}