<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rolemenu extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('dashboard');
	}

	public function kelolamenu(){
		$data['dataMenu'] = $this->db->get_where('admin_menu_items',array('role_id !=' => 0))->result();
		$this->load->view('v_kelolamenu', $data);
	}

	public function tambahmenu(){
		$data['parent'] = $this->db->get_where('admin_menu_items',array('parent' => 0))->result();
		$data['role'] = $this->db->get('role')->result();
		$this->load->view('v_tambahmenu',$data);
	}

	public function simpanmenu(){
		$data = array(
			'label' => $this->input->post('label'),
			'link' => $this->input->post('link'),
			'parent' => $this->input->post('parent'),
			'role_id' => $this->input->post('role'),
			'created_at' => 'NOW()',
			'updated_at' => 'NOW()'
		);
	
		$this->db->insert('admin_menu_items', $data);
		redirect('rolemenu/tambahmenu');
	}

	public function kelolapengguna(){
		$data['dataMenu'] = $this->db->get('user')->result();
		$this->load->view('v_listPengguna', $data);
	}

	public function kelolaakses(){
		$data['idUser'] = $_GET['id'];
		$data['username'] = $this->db->get_where('user',array('id' => $data['idUser']))->row('username');
		// $this->db->get('admin_menu_items')->result();
		$data['dataAkses'] = $this->db->get_where('akses_pengguna',array('id_role' => $data['idUser']))->result();

		$menu = array();
		foreach ($data['dataAkses'] as $key) {
		 	$menu[]	= $key->id_menu;
		}
		if(!empty($menu)){
			$data['dataMenuIn'] = $this->Model_rolemenu->getMenuIn($menu);
			$data['dataMenuOut'] = $this->Model_rolemenu->getMenuNotIn($menu);
		}else{
			$data['dataMenuIn'] = 0;
			$data['dataMenuOut'] = $this->Model_rolemenu->getMenu();
		}

		$this->load->view('v_aksesPengguna', $data);
	}

	public function prosesAkses($id){
		#delete existing
		$this->db->where('id_role', $id);
		$this->db->delete('akses_pengguna');

		foreach ($_POST as $key) {
			#insert akses_pengguna
			$data = array(
				'id_role' => $id,
				'id_menu' => $key
			);
		
			$this->db->insert('akses_pengguna', $data);
		}
		redirect('rolemenu/kelolapengguna');
		// array(4) { [45]=> string(2) "45" [46]=> string(2) "46" [47]=> string(2) "47" [48]=> string(2) "48" }
	}
}
