<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('v_login');
	}

	public function prosesLogin(){
		// var_dump($_POST);
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$getUser = $this->db->get_where('user', array('username' => $username, 'password' => $password))->row();
		if(isset($getUser)){
				$sessionData = array(
					'username'  => $getUser->username,
					'role'     => $getUser->role,
					'logged_in' => TRUE
				);
				$this->session->set_userdata($sessionData);
				redirect('Rolemenu');
		}else{
			redirect('Login');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}
}
