<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Kelola Menu </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="rolemenu/kelolamenu">Kelola Menu</a></li>
                </ol>
              </nav>
            </div>
            <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Tambah Menu</h4>
                    <p class="card-description">  </p>
                    <form class="forms-sample" action="simpanMenu" method="POST">
                      <div class="form-group">
                        <label for="exampleInputName1">Label</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Label" name="label" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail3">Link</label>
                        <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Link" name="link" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleSelectGender">Parent</label>
                        <select class="form-control" id="exampleSelectGender" name="parent">
                          <option value="">Pilih Parent</option>
                          <?php foreach ($parent as $pData) {
                            echo "<option value=".$pData->id.">".$pData->label."</option>";
                          }?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleSelectGender">Role</label>
                        <select class="form-control" id="exampleSelectGender" name="role" required>
                        <option value="">Pilih Role</option>
                        <?php foreach ($role as $rData) {
                            echo "<option value=".$rData->id.">".$rData->nama_role."</option>";
                          }?>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-gradient-primary mr-2">Simpan</button>
                      <button class="btn btn-light">Batal</button>
                    </form>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <?php $this->load->view('footer');?>