<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Kelola Menu </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="rolemenu/kelolamenu">Kelola Menu</a></li>
                </ol>
              </nav>
            </div>
            <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Daftar Menu | <a href="TambahMenu"><button class="btn btn-gradient-primary btn-sm font-weight-medium auth-form-btn">Tambah Menu</button></a></h4>
                    <p class="card-description"> Add class <code>.table-bordered</code>
                    </p>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th> Label </th>
                          <th> Link </th>
                          <th> Parent </th>
                          <th> Role </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1;foreach ($dataMenu as $data) { ?>
                            <tr>
                                <td> <?php echo $no++;?> </td>
                                <td> <?php echo $data->label;?> </td>
                                <td> <?php echo $data->link;?> </td>
                                <td> <?php echo $data->parent;?> </td>
                                <td> <?php echo $data->role_id;?> </td>
                            </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <?php $this->load->view('footer');?>