<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="<?php echo base_url('assets');?>/images/faces/face1.jpg" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2">David Grey. H</span>
                  <span class="text-secondary text-small">Project Manager</span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>
            <?php 
                $role = $this->session->userdata('role');
                $dataMenu = $this->Model_rolemenu->get_akses_menu(0,$role);
                foreach ($dataMenu as $data) { 
                    $dataChild = $this->Model_rolemenu->get_akses_menu_child($data->id);
                    if(count($dataChild) > 0){
            ?>
                        <li class="nav-item">
                        <a class="nav-link" data-toggle="collapse" href="#<?php echo str_replace(' ','',$data->label);?>" aria-expanded="false" aria-controls="ui-basic">
                            <span class="menu-title"><?php echo $data->label;?></span>
                            <i class="menu-arrow"></i>
                            <!-- <i class="mdi mdi-crosshairs-gps menu-icon"></i> -->
                        </a>
                        <div class="collapse" id="<?php echo str_replace(' ','',$data->label);?>">
                            <ul class="nav flex-column sub-menu">
                            <?php 
                                foreach ($dataChild as $cData) { ?>
                                    <li class="nav-item"> <a class="nav-link" href="<?php echo $cData->link;?>"><?php echo $cData->label;?></a></li>  
                            <?php    
                                }    
                            ?>
                            </ul>
                        </div>
                        </li>

                <?php 
                    } else {
                ?>
                <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url()."index.php/".$data->link;?>">
                    <span class="menu-title"><?php echo $data->label;?></span>
                    <i class="mdi mdi-home menu-icon"></i>
                </a>
                </li>
            <?php
                    }
                }
            ?>
          </ul>
        </nav>