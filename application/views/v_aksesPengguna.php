<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Kelola Akses </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="rolemenu/kelolaakses">Kelola Akses</a></li>
                </ol>
              </nav>
            </div>
            <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Daftar Akses : <?php echo $username;?></h4>
                    <p class="card-description"> Add class <code>.table-bordered</code>
                    </p>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> Menu </th>
                        </tr>
                      </thead>
                      <form action="<?php echo base_url('index.php/rolemenu/prosesAkses')."/".$idUser;?>" method="POST">
                        <tbody>
                            
                                <tr>
                                    <td > 
                                        <?php 
                                        if($dataMenuIn != 0){
                                            foreach ($dataMenuIn as $data) {
                                                    echo "<input type='checkbox' id='".$data->id."' name='".$data->id."' value='".$data->id."' checked>&nbsp".$data->label."<br>";
                                            } 
                                        }
                                        ?>
                                        <?php foreach ($dataMenuOut as $data) {
                                                echo "<input type='checkbox' id='".$data->id."' name='".$data->id."' value='".$data->id."'>&nbsp".$data->label."<br>";
                                        } ?>
                                    </td>
                                </tr>
                            
                        </tbody>
                        </table>
                        <br>
                        <button type = "submit" class="btn btn-gradient-primary btn-sm font-weight-medium auth-form-btn float-right" >Submit Akses</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kelola Akses : <div id="nama"></div></h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>

            </div>
            </div>
            <!-- end modal -->
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <?php $this->load->view('footer');?>
          