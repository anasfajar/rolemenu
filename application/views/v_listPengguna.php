<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Kelola Pengguna </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="rolemenu/kelolapengguna">Kelola Pengguna</a></li>
                </ol>
              </nav>
            </div>
            <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Daftar Pengguna | <a href="TambahMenu"><button class="btn btn-gradient-primary btn-sm font-weight-medium auth-form-btn">Tambah Pengguna</button></a></h4>
                    <p class="card-description"> Add class <code>.table-bordered</code>
                    </p>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th> Username </th>
                          <th class="text-center"> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1;foreach ($dataMenu as $data) { ?>
                            <tr>
                                <td> <?php echo $no++;?> </td>
                                <td> <?php echo $data->username;?> </td>
                                <td class="text-center"> <a href="<?php echo base_url()."index.php/rolemenu/kelolaakses?id=".$data->id;?>"><button class="btn btn-gradient-info">Akses</button> </a></td>
                                <!-- <td class="text-center"> <button data-toggle="modal" data-target="#myModal" class="btn btn-gradient-info">Akses</button> </td> -->
                                
                            </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kelola Akses : <div id="nama"></div></h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </div>

            </div>
            </div>
            <!-- end modal -->
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <?php $this->load->view('footer');?>
          